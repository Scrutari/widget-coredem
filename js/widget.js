/***************************************************************
* Coredem.info Scrutari Widget - Web resources display based on Scrutari search Engine
* http://www.scrutari.net/ - http://www.coredem.info
*
* Copyright (c) 2015-2016 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/* global Scrutari */

Widget = function (scrutariConfig, locMap, type, langList) {
    this.scrutariConfig = scrutariConfig;
    this.scrutariLoc = new Scrutari.Loc(locMap);
    this.type = type;
    this.langList = langList;
    this.initialQuery = false;
    this.motcle = false;
    this.baseList = false;
    this.waiting = false;
    this.scrutariMeta = null;
    this.target = "_blank";
    this.qId = null;
};

Widget.prototype.init = function (scrutariMeta) {
    var widget = this;
    var scrutariConfig = this.scrutariConfig;
    this.scrutariMeta = scrutariMeta;
    var requestParameters = new Object();
    if (this.type === 'qid') {
        requestParameters["qid"] = this.qId;
        requestParameters["motclefields"] = "mlibelles";
        requestParameters["starttype"] = "in_all";
        requestParameters["start"] = "1";
        requestParameters["limit"] = "30";
        
        var _qIdCallback = function (ficheSearchResult) {
            Widget.scrutariResult(widget, new Scrutari.Result(ficheSearchResult, scrutariConfig.getGroupSortFunction()));
        };
        Scrutari.Ajax.loadExistingFicheSearchResult(_qIdCallback, scrutariConfig, requestParameters); 
        return;
    }
    var _scrutariResultCallback = function (scrutariResult) {
        Widget.scrutariResult(widget, scrutariResult);
    };
    var _scrutariErrorCallback = function (error) {

    };
    if (this.type === 'query') {
        requestParameters["q"] = this.initialQuery;
        requestParameters["baselist"] = "/e17a05b0-c45e-11d8-9669-0800200c9a66/ess";
    } else if (this.type === 'motcle') {
        requestParameters["q"] = "*";
        requestParameters["q-mode"] = "operation";
        requestParameters["flt-motcle"] = "/e17a05b0-c45e-11d8-9669-0800200c9a66/ess/" + this.motcle;
        requestParameters["baselist"] = "/e17a05b0-c45e-11d8-9669-0800200c9a66/ess";
    } else if (this.type === 'base') {
        requestParameters["q"] = "*";
        requestParameters["q-mode"] = "operation";
        requestParameters["baselist"] = this.baseList;
    } 
    requestParameters["langlist"] = this.langList;
    Scrutari.Result.newSearch(_scrutariResultCallback, scrutariConfig, requestParameters, _scrutariErrorCallback);
};

Widget.prototype.setInitialQuery = function (initialQuery) {
    this.initialQuery = initialQuery;
};

Widget.prototype.setMotcle = function (motcle) {
    this.motcle = motcle;
};

Widget.prototype.setBaseList = function (baseList) {
    this.baseList = baseList;
};

Widget.prototype.setQId = function (qId) {
    this.qId = qId;
};

Widget.prototype.getScrutariConfig = function () {
    return this.scrutariConfig;
};

Widget.prototype.isWaiting = function () {
    return this.waiting;
}

Widget.prototype.setWaiting = function (bool) {
    this.waiting = bool;
};

Widget.prototype.getScrutariMeta = function () {
    return this.scrutariMeta;
};

Widget.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};

Widget.prototype.getTarget = function () {
    return this.target;
};

Widget.scrutariResult = function (widget, scrutariResult) {
    var $ficheDisplayBlock = $("#ficheDisplayBlock");
    $ficheDisplayBlock.empty();
    if (scrutariResult.getFicheGroupType() === 'category') {
        var categoryCount = scrutariResult.getCategoryCount();
        var tabHtml = '<ul class="nav nav-tabs" role="tablist">';
        var contentHtml = "<div class='tab-content'>";
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            tabHtml += "<li";
            contentHtml += "<div class='tab-pane";
            if (i == 0) {
                tabHtml += " class='active'";
                contentHtml += " active";
            }
            tabHtml += "><a class='scrutari-Onglet' role='tab' data-toggle='tab' href='#categoryTabContent_"
                + category.name
                + "'>"
                + Scrutari.Utils.escape(category.title)
                + " ("
                + scrutariResult.getCategoryFicheCount(i)
                + ")"
                + "</a></li>";
            contentHtml += "' id='categoryTabContent_" + category.name + "'>";
            contentHtml += "<ul class='pagination scrutari-Pagination_" + category.name + "'></ul>";
            contentHtml += "<div id='categoryFiches_" + category.name + "'></div>"
            contentHtml += "<ul class='pagination scrutari-Pagination_" + category.name + " scrutari-BottomPagination'></ul>";
            contentHtml += "</div>";
        }
        tabHtml += "</ul>";
        contentHtml += "</div>";
        $ficheDisplayBlock.html(tabHtml + contentHtml);
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            Widget.categoryChangePlage(widget, category.name, scrutariResult, widget.getScrutariConfig().getPlageLength(), 1);
        }
    } else {
        $ficheDisplayBlock.html("<ul class='pagination scrutari-Pagination'></ul><div id='noneFiches'></div><ul class='pagination scrutari-Pagination scrutari-BottomPagination'></ul>");
        Widget.noneChangePlage(widget, scrutariResult, widget.getScrutariConfig().getPlageLength(), 1);
    }
};

Widget.noneChangePlage = function (engine, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isNonePlageLoaded(plageLength, plageNumber)) {
        if (engine.isWaiting()) {
            return;
        }
        $("#noneFiches").html("<span class='scrutari-icon-Loader'></span> " + engine.getScrutariLoc().loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Widget.noneChangePlage(engine, scrutariResult, plageLength, plageNumber);
        };
        engine.setWaiting(true);
        scrutariResult.loadNonePlage(_plageCallback, engine.getScrutariConfig(), plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectNoneFicheArray(plageLength, plageNumber);
    var ficheHtmlFunction =  Scrutari.Html.initFicheHtmlFunction(engine.getScrutariMeta(), engine.getScrutariLoc(), scrutariResult, engine.getTarget());
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheHtmlFunction(plageFicheArray[i]);
    }
    $("#noneFiches").html(html);
    var paginationIdPrefix = "pagination_";
    Scrutari.Utils.checkPagination(scrutariResult.getFicheCount(), plageLength, plageNumber, ".scrutari-Pagination", Scrutari.Html.initPaginationHtmlFunction(paginationIdPrefix));
    $(".scrutari-Pagination a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Widget.noneChangePlage(engine, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination.scrutari-BottomPagination a").click(function () {
        Widget.scrollToResult();
    });
};
    
Widget.categoryChangePlage = function (engine, categoryName, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isCategoryPlageLoaded(categoryName, plageLength, plageNumber)) {
        $("#categoryFiches_" + categoryName).html("<span class='scrutari-icon-Loader'></span> " + engine.getScrutariLoc().loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Widget.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, plageNumber);
        };
        if (engine.isWaiting()) {
            return;
        }
        engine.setWaiting(true);
        scrutariResult.loadCategoryPlage(_plageCallback, engine.getScrutariConfig(), categoryName, plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectCategoryFicheArray(categoryName, plageLength, plageNumber);
    var ficheHtmlFunction =  Scrutari.Html.initFicheHtmlFunction(engine.getScrutariMeta(), engine.getScrutariLoc(), scrutariResult, engine.getTarget());
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheHtmlFunction(plageFicheArray[i]);
    }
    $("#categoryFiches_" + categoryName).html(html);
    var paginationIdPrefix = "pagination_" + categoryName + "_";
    Scrutari.Utils.checkPagination(scrutariResult.getCategoryFicheCountbyName(categoryName), plageLength, plageNumber, ".scrutari-Pagination_" + categoryName, Scrutari.Html.initPaginationHtmlFunction(paginationIdPrefix));
    $(".scrutari-Pagination_" + categoryName + " a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Widget.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination_" + categoryName + ".scrutari-BottomPagination a").click(function () {
        Widget.scrollToResult();
    });
};

Widget.scrollToResult = function () {
    $(window).scrollTop($("#ficheDisplayBlock").offset().top);
};

