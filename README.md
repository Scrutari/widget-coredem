# Insertion de données issues de Scrutari dans le site de la Coredem

L'idée de cette application est de valoriser le contenu des participants de la Coredem directement sur le site de la Coredem en utilisant Scrutari.

Cette valorisation se fait de deux manières :
- en affichant le contenu pour un site particulier (par exemple, Autour du premier mai : http://www.coredem.info/article24.html)
- en proposant pour un sujet donné, une recherche prédéfinie (par exemple, pour le numéro de Passerelles sur le climat, il propose le résultat de la recherche sur le thème « climat et transition énergétique » : http://www.coredem.info/rubrique67.html)

Ce contenu est inséré à l'intérieur des pages à l'aide d'une balise <iframe> qui appelle l'application (elle-même accessible via http://apps.coredem.info/widget/).

