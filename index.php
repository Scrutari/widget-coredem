<?php
/***************************************************************
* Coredem.info Scrutari Widget - Web resources display based on Scrutari search Engine
* http://www.scrutari.net/ - http://www.coredem.info
*
* Copyright (c) 2015-2016 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

$GLOBALS['scrutari'] = array();
$GLOBALS['scrutari']['engine'] = array();
initQuery();
initEngineOptions();
initLangui();
initOrigin();
initFicheFields();


function initQuery() {
    if(isset($_REQUEST['q'])) {
        $q = trim($_REQUEST['q']);
        if (strlen($q) > 0) {
            $GLOBALS['scrutari']['query'] = $q;
            $GLOBALS['scrutari']['type'] = 'query';
        }
    } else if(isset($_REQUEST['motcle'])) {
        $motcle = trim($_REQUEST['motcle']);
        if (strlen($motcle) > 0) {
            $GLOBALS['scrutari']['motcle'] = $motcle;
            $GLOBALS['scrutari']['type'] = 'motcle';
        }
    } else if(isset($_REQUEST['base'])) {
        $base = trim($_REQUEST['base']);
        if (strlen($base) > 0) {
            $GLOBALS['scrutari']['base'] = $base;
            $GLOBALS['scrutari']['type'] = 'base';
        }
    } else if (isset($_REQUEST['qid'])) {
        $qid = trim($_REQUEST['qid']);
        if (strlen($qid) > 0) {
            $GLOBALS['scrutari']['qid'] = $qid;
            $GLOBALS['scrutari']['type'] = 'qid';
        }
    }
}

function initLangui() {
    $langui = 'fr';
    $l10n = 'fr';
    $langlist = '';
    //initLoc('fr');
    if (isset($_REQUEST['langui'])) {
        $languiParam = $_REQUEST['langui'];
        if (preg_match('/^[-a-zA-Z_]+$/', $languiParam)) {
            $langui = $languiParam;
            if ($langui != 'fr') {
                /*initLoc($langui);*/
                if (file_exists("scrutarijs/js/l10n/".$langui.".js")) {
                    $l10n = $langui;
                }
            }
        }
        if (!isset($_REQUEST['langlist'])) {
            $langlist = $langui;
        }
    }
    if (isset($_REQUEST['langlist'])) {
        $langlist = $_REQUEST['langlist'];
    }
    $GLOBALS['scrutari']['langui'] = $langui;
    $GLOBALS['scrutari']['l10n'] = $l10n;
    $GLOBALS['scrutari']['langlist'] = $langlist;
}

function initEngineOptions() {
    $GLOBALS['scrutari']['engine']['name'] = "coredem";
    $GLOBALS['scrutari']['engine']['url'] = "http://sct1.scrutari.net/sct/coredem/";
}

function initOrigin() {
    $GLOBALS['scrutari']['origin'] = 'widget';
}

function initFicheFields() {
    $fields = "codecorpus,mtitre,msoustitre,mcomplements,annee,href";
    $withAttrs = true;
    if(isset($_REQUEST['attrs'])) {
        $attrs = trim($_REQUEST['attrs']);
        if ($attrs == 'false') {
            $withAttrs = false;
        }
    }
    if ($withAttrs) {
        $fields .= ",mattrs_all";
    }
    if ($GLOBALS['scrutari']['type'] != 'base') {
        $fields .= ",icon";
    }
    $GLOBALS['scrutari']['fields'] = $fields;
}

?>
<!DOCTYPE html>
<html lang="<?php echo $GLOBALS['scrutari']['langui'];?>">
<head>
<title>Widget Coredem</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="jquery/1.11.2/jquery.min.js"></script>
<script src="bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/widget.css">
<script src="js/scrutari.js"></script>
<script src="js/widget.js"></script>
<script>
    Engine = function() {}; //nécessaire pour le chargement de l10n qui crèe Engine.l10n
</script>
<script src="js/l10n/<?php echo $GLOBALS['scrutari']['l10n']; ?>.js"></script>
<style>
#ficheDisplayBlock div.scrutari-fiche-Block {
    border-left-style: none;
}
</style>

<script>
    
    $(function () {
        var scrutariConfig = new Scrutari.Config("<?php echo $GLOBALS['scrutari']['engine']['name'];?>","<?php echo $GLOBALS['scrutari']['engine']['url'];?>", "<?php echo $GLOBALS['scrutari']['langui'];?>", "<?php echo $GLOBALS['scrutari']['origin'];?>");
        scrutariConfig.setFicheFields("<?php echo addslashes($GLOBALS['scrutari']['fields']);?>");
        scrutariConfig.setPlageLength(5);
        var widget = new Widget(scrutariConfig, Engine.l10n, "<?php echo addslashes($GLOBALS['scrutari']['type']);?>", "<?php echo addslashes($GLOBALS['scrutari']['langlist']);?>");
        <?php if ($GLOBALS['scrutari']['type'] == 'motcle') { ?>
        widget.setMotcle("<?php echo addslashes($GLOBALS['scrutari']['motcle']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'query') { ?>
        widget.setInitialQuery("<?php echo addslashes($GLOBALS['scrutari']['query']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'base') { ?>
        widget.setBaseList("<?php echo addslashes($GLOBALS['scrutari']['base']);?>");
        <?php } else if ($GLOBALS['scrutari']['type'] == 'qid') { ?>
        widget.setQId("<?php echo addslashes($GLOBALS['scrutari']['qid']);?>");
        <?php } ?>
        var _scrutariMetaCallback = function (scrutariMeta) {
           widget.init(scrutariMeta);
        };
        Scrutari.Meta.load(_scrutariMetaCallback, scrutariConfig);
    });
</script>

</head>
<body>
<div class="container" id="mainContainer">
    <div class="row">
        <div class="col-sm-7 col-sm-push-2" id="ficheDisplayBlock">
        </div>
    </div>
</div>
</body>
</html>